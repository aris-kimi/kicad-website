+++
title = "Gerber Viewer"
tags = ["gerbers", "gerbview", "rs274x", "gerber x2"]
[menu.main]
    parent = "Discover"
    name   = "Gerber Viewer"
	weight = 4
+++

View your gerbers before sending out to manufacturing or just general review
of third party gerbers. 

<!--more-->

== Load gerbers

Load any amount of gerber layers

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="load-gerbers.webm" type="video/webm">
        <source src="load-gerbers.mp4" type="video/mp4">
    </video>
</div>
++++

== X2 Format Support

Load Gerber X2 job files (.gbrjob) and identify components and netlists easily with the loaded X2 metadata.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="x2-attr.webm" type="video/webm">
        <source src="x2-attr.mp4" type="video/mp4">
    </video>
</div>
++++

== Use inspection features

A variety of inspection helpers for gerbers are available. Measure clearances, view and compare layer elements.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="inspection.webm" type="video/webm">
        <source src="inspection.mp4" type="video/mp4">
    </video>
</div>
++++

== Export to KiCad PCB

Convert gerbers to a KiCad PCB project for editing. A powerful tool for designs whose only available source may be gerbers.

NOTE: Converting to gerbers will not create a normal KiCad PCB design, it will be editable representation of the copper from the gerber.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="export-pcb.webm" type="video/webm">
        <source src="export-pcb.mp4" type="video/mp4">
    </video>
</div>
++++